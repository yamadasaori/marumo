//flexibility
document.addEventListener("DOMContentLoaded", function () {
  var ua = navigator.userAgent;
  if (ua.indexOf("Android 4.") > 0) {
    flexibility(document.body);
  }
});


//ブレイクポイント
var xs = 0,
sm = 576,
md = 768,
lg = 992,
xl = 1200;



jQuery(function(){
//何か共通で使用するjsがあればこちらに
	//カレント用
	var currentUrl = location.href;
	$('body a').each(function(){
		var hrefUrl = $(this).prop("href");
		if(currentUrl == hrefUrl){
			$(this).addClass('current');
		}
	});

	//スクロール用
   $('a[href^=#]').click(function() {
      var speed = 400; // ミリ秒
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });

   $('#spNavbtn').click(function() {
      $('.spNav').fadeIn('fast');
   });

   $('.closebtn').click(function() {
      $('.spNav').fadeOut('fast');
   });


   //スマホ時メニューをスライドさせる
   if($(window).width()<lg){
     $('.havechild > a').each(function() {
        $(this).click(function(event) {
            event.preventDefault();
          $(this).parent('li').children('ul').slideToggle();   
        });     
     });
   }

   //ロールオーバー時 .currentもメニューは消す
   if($(window).width()>lg){
     $('.glovalNav .havechild').hover(function() {
      $('.glovalNav .current ul').css('display','none');
     }, function() {
      $('.glovalNav .current ul').css('display','block');
     });
     $('.glovalNav .havechild.current').hover(function() {
      $('.glovalNav .current ul').css('display','block');
     });
   }

   $('.glovalNav ul li').each(function() {
    if($(this).hasClass('havechild') && $(this).hasClass('current')){
      $('.entryHeader').css('margin-top','10rem');
    }
     
   });



});



//ウィンドウをリサイズかつロードした場合
$(window).on('load resize', function(){
// 処理を記載
});